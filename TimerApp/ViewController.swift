//
//  ViewController.swift
//  TimerApp
//
//  Created by yash yadav on 3/3/16.
//  Copyright © 2016 yash yadav. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var timer = NSTimer();
    var t=0;
    var timerOff = true;
    @IBOutlet weak var textTime: UITextField!
    @IBAction func stop(sender: AnyObject) {
        timer.invalidate()
        timerOff=true
    }
    @IBAction func start(sender: AnyObject) {
        if(timerOff){
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "increaseTimer", userInfo: nil, repeats: true)
            timerOff=false
        }
    }
    @IBAction func reset(sender: AnyObject) {
        t=0;
        textTime.text = "0"
        timer.invalidate()
        timerOff=true
    }
    
    func increaseTimer(){
        t++;
        textTime.text = String(t)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

